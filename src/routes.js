import Home from './components/Home/Home.vue'
import AddItem from './components/Home/AddItem'
import SignUp from './components/Auth/SignUp.vue'
import SignIn from './components/Auth/SignIn.vue'

const routes = [
  {path:'/', component:Home, name:'home'},
  {path:'/addItem', component:AddItem, name:'addItem'},
  {path:'/signup', component:SignUp, name:'signUp'},
  {path:'/signin', component:SignIn, name:'signIn'},
];

export default routes;
