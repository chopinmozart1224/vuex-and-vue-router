import Vuex from 'vuex';
import Vue from 'vue'
import _ from 'lodash'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts:[]
  },
  getters: {
    havePosts: state => {
      return state.posts.length > 0 ? true : false
    }
  },
  mutations: {
    setPost(state, postData){
      state.posts =[...state.posts, postData]
    },
    removePost(state, postId){
      state.posts = _.filter(state.posts, (post)=>{
        return post.id!==postId
      })
    },
    updatePost(state, updateObject){
      let {postId, name, fruits, color, updatedMessage } = updateObject
      let index = _.findIndex(state.posts, {id:postId})
      state.posts.splice(index, 1, {id: postId, name, message:updatedMessage, fruits, color});
    }
  },
  actions: {
    setPost(context, postData){
      context.commit('setPost',postData)
    },
    removePost(context, postId){
      context.commit('removePost', postId)
    },
    updatePost(context, updateObject){
      context.commit('updatePost', updateObject)
    }
  }
});
